#include "rect.hpp"

//Public
Rect2::Rect2() {
	this->x = 0;
	this->y = 0;
	this->width = 0;
	this->height = 0;
	this->direction = 0;
}

Rect2::Rect2(float x, float y, float direction, float width, float height) {
	this->x = x;
	this->y = y;
	this->direction = direction;
	this->width = width;
	this->height = height;
}

Rect2::Rect2(Pol2 p) {
	*this = toRect(p);
}

bool Rect2::operator<(const Rect2& r) const {
	return this->norm2() < r.norm2();
}

bool Rect2::operator>(const Rect2& r) const {
	return this->norm2() > r.norm2();
}

bool Rect2::operator<(const double& n) const {
	return this->norm2() < n;
}
bool Rect2::operator>(const double& n) const {
	return this->norm2() > n;
}

Rect2& Rect2::operator+=(const Rect2& pts) {
	this->x += pts.x;
	this->y += pts.y;

	return *this; //return the result by reference
}

Rect2 Rect2::operator+(const Rect2& r_) const {
	return Rect2(r_.x + this->x, r_.y + this->y);
}

Rect2 Rect2::operator-(const Rect2& r_) const {
	return Rect2(this->x - r_.x, this->y - r_.y,this->direction - r_.direction);
}

float Rect2::norm1() const {
	return fabs(x) + fabs(y);
}

float Rect2::norm2() const {
	return sqrt(x*x + y*y);
}

//Private

Rect2 Rect2::toRect(Pol2 p) {
	return Rect2(p.rho * cos(p.angle), p.rho * sin(p.angle), p.direction);
}

Pol2 Rect2::toPol(Rect2 r) {
	Pol2 p = Pol2(0, 0);

	p.rho = sqrt(r.x * r.x + r.y * r.y);
	p.angle = atan2(r.y, r.x);
	p.direction = r.direction;

	if(p.angle < 0) p.angle += 2*M_PI;

	return p;
}
