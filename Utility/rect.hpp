#ifndef RECT_H
#define RECT_H

#include <stdio.h>
#include <math.h>

#include "pol.hpp"
#include "../const.hpp"

class Pol2;

class Rect2 {
	public :
		float x;
		float y;
		float width;
		float height;
		float direction;

		Rect2();
		Rect2(float x, float y, float direction = 0, float width = 0, float height = 0);
		Rect2(Pol2 p);

		bool operator<(const Rect2& r) const;
		bool operator<(const double& n) const;
		bool operator>(const Rect2& r) const;
		bool operator>(const double& n) const;

		Rect2& operator+=(const Rect2& pts);

		Rect2 operator-(const Rect2& r_) const;
		Rect2 operator+(const Rect2& r_) const;

		float norm1() const;
		float norm2() const;

		void toString() {
			printf("Rect2 (%f %f | %f)\n", x, y, direction*TO_DEG);
		};

	private :
		Rect2 toRect(Pol2 p);
		Pol2 toPol(Rect2 r);
};

inline void writeRect2VectorInFile(std::vector<Rect2> r, std::string dest) {
	std::ofstream myfile;
	myfile.open (dest);

	for(int i = 0; i < r.size(); i++) {
		myfile << r[i].x << " " << r[i].y << std::endl;
	}

	myfile.close();
}

#endif
