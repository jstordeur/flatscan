#include "flatscan.hpp"

using namespace std;

Flatscan::Flatscan() {
	this->portcom = DEFAULT_PORTCOM;
	this->baudrate = DEFAULT_BAUDRATE;

	//Default config
	this->config.Measurement_Mode = SINGLE_SHOT;
	this->config.CTN = DISABLE;
	this->config.MDI_Mode = DISTANCE;
	this->config.Detection_Mode = HD;
	this->config.Sensitivity = 3;
	this->config.NbrSpots = 400;
	this->config.AngleFirst = 0;
	this->config.AngleLast = 10800;
	this->config.CAN_FrameCntr = ENABLE;
	this->config.HeartBeatPeriod = 0;
	this->config.Facet_Nbr = ENABLE;
	this->config.Averaging = 4;

	update_Config();

	this->frame = new Frame();
}

Flatscan::Flatscan(int portcom) : Flatscan() {
	this->portcom = portcom;
}

Flatscan::Flatscan(int portcom, int baudrate) : Flatscan(portcom) {
	this->baudrate = baudrate;
}

Flatscan::~Flatscan(){
	delete frame;
}

int Flatscan::get_CanNumber(bool needUpdate) {
	if(needUpdate) {
		this->get_Identity(nullptr);
	}
	return this->canNumber;
}

bool Flatscan::connect() {
	char mode[] = { '8', 'N', '1', '\0' };

	if(RS232_OpenComport(portcom, baudrate, mode))
	{
		if(MASTER_DEBUG && DEBUG_FLAT){ printf("\n%s : Cannot open comport %d.\n", PREFIX_FLATSCAN, portcom); }
		throw string("Cannot open comport.");
	}
	if(MASTER_DEBUG && DEBUG_FLAT) { printf("\n%s : Comport successfully connected \n->CONFIGURATION : \n\tPort number = %d \n\tBaudrate = %d \n\tMode = %s \n", PREFIX_FLATSCAN, portcom, baudrate, mode); }
	RS232_flushRXTX(portcom);
	return true;
}

void Flatscan::close() {
	RS232_CloseComport(portcom);
	if(MASTER_DEBUG && DEBUG_FLAT) { printf("\n%s : Comport disconnected. \n", PREFIX_FLATSCAN); }
}

void Flatscan::init() {
	update_CAN(); //By-pass tour to be able to know the CAN number. Because get_Identity don't seems to work

	set_Led(IS_INIT);
}

bool Flatscan::update_Config() {
	this->angleStep = (float)(this->config.AngleLast/100 - this->config.AngleFirst/100) / (float)this->config.NbrSpots * TO_RAD;

	switch(this->config.MDI_Mode)
	{
		case DISTANCE :
			this->distance.reserve(this->config.NbrSpots * 2);
			this->remission.reserve(0);
			break;
		case REMISSION :
			this->distance.reserve(0);
			this->remission.reserve(this->config.NbrSpots * 2);
			break;
		case DISTANCE_REMISSION :
			this->distance.reserve(this->config.NbrSpots * 2);
			this->remission.reserve(this->config.NbrSpots * 2);
			break;
		default :
			printf("Geez, read the man ! You, dumb !\n");
	}

	return true;
}

bool Flatscan::update_CAN() {
	this->config.CAN_FrameCntr = ENABLE;
	this->set_Parameters(this->config);

    this->get_Measurments(false);

	this->canNumber = this->header[0]
		+ this->header[1] * 8
		+ this->header[2] * 64
		+ this->header[3] * 512;

	this->config.CAN_FrameCntr = DISABLE;
	this->set_Parameters(this->config);
}

bool Flatscan::get_Identity(Identity* id) {
	//Build CMD
	frame->cmd.clear();
	frame->cmd.push_back(0x5A);
	frame->cmd.push_back(0xc3);

	//Build DATA
	frame->data.clear();

	//Build SYNC - after DATA because we need to know the size of frame
	frame->computeSync();

	//Build CHK - crc16
	frame->computeCHK();

	//Send&Read
	try{
		Command::sendFrameReadAck(this);
	} catch(std::string e){
		std::cout << e << std::endl;
	}

	//Mean
	this->canNumber = frame->data[7] + frame->data[8] * pow(2, 8) + frame->data[9] * pow(2, 16) + frame->data[10]* pow(2, 32);

	if(id != nullptr) {
		id->partNumber = frame->data[0] + frame->data[1] * pow(2, 8) + frame->data[2] * pow(2, 16) + frame->data[3]* pow(2, 32);
		id->softwareVersion = frame->data[4];
		id->softwareRevision = frame->data[5];
		id->softwarePrototype = frame->data[6];
		id->canNumber = this->canNumber;
	}

	return true;
}

bool Flatscan::set_Baudrate(int baudrate) {

	//Build CMD
	frame->cmd.clear();
	frame->cmd.push_back(0x51);
	frame->cmd.push_back(0xc3);

	//Build DATA
	frame->data.clear();
	switch (baudrate) {
	case 57600: frame->data.push_back(0x00); break;
	case 115200: frame->data.push_back(0x01); break;
	case 230400: frame->data.push_back(0x02); break;
	case 460800: frame->data.push_back(0x03); break;
	case 921600: frame->data.push_back(0x04); break;
	default: throw string("Invalide baudrate.\n");
			break;
  }

	//Build SYNC - after DATA because we need to know the size of frame
	frame->computeSync(); //0x10

	//Build CHK - crc16
	frame->computeCHK();

	//Send&Read
	try{
		Command::sendFrameReadAck(this);
	} catch(std::string e){
		std::cout << e << std::endl;
	}

	//Mean

	return true;
}

bool Flatscan::get_Parameters() {

	//Build CMD
	frame->cmd.clear();
	frame->cmd.push_back(0x54);
	frame->cmd.push_back(0xC3);

	//Build Data
	frame->data.clear();

	//Build SYNC - after DATA because we need to know the size of frame
	frame->computeSync();

	//Build CHK - crc16
	frame->computeCHK();

	//Send&Read
	try{
		Command::sendFrameReadAck(this);
	} catch(std::string e){
		std::cout << e << std::endl;
	}

	return true;
}

bool Flatscan::set_Parameters(Config config) {
	//Build CMD
	frame->cmd.clear();
	frame->cmd.push_back(0x53);
	frame->cmd.push_back(0xc3);

	//Build DATA - O M G
	frame->data.clear();
	frame->data.push_back(0x00);
	frame->data.push_back(config.CTN);
	frame->data.push_back(config.MDI_Mode);
	frame->data.push_back(config.Detection_Mode);
	frame->data.push_back(config.Sensitivity);
	frame->data.push_back(0x00);//5
	frame->data.push_back(0x00);
	frame->data.push_back(0x00);
	frame->data.push_back(config.NbrSpots%256);
	frame->data.push_back(config.NbrSpots/256);
	frame->data.push_back(0x00);//10
	frame->data.push_back(0x00);
	frame->data.push_back(0x00);
	frame->data.push_back(0x00);
	frame->data.push_back(config.AngleFirst%256);
	frame->data.push_back(config.AngleFirst/256);//15
	frame->data.push_back(config.AngleLast%256);
	frame->data.push_back(config.AngleLast/256);
	frame->data.push_back(config.CAN_FrameCntr);
	frame->data.push_back(config.HeartBeatPeriod);
	frame->data.push_back(config.Facet_Nbr);//20
	frame->data.push_back(config.Averaging);

	//Build SYNC - after DATA because we need to know the size of frame
	frame->computeSync();

	//Build CHK - crc16
	frame->computeCHK();

	//Send&Read
	try{
		Command::sendFrameReadAck(this);
	} catch(std::string e){
		std::cout << e << std::endl;
	}

	//Verify the integrity of set_parameters
	if(frame->data[0] != 0 || frame->data[1] != 0 || frame->data[2] != 0 || frame->data[2] != 0) throw string("Set parameters ack bits error\n");

	this->config = config;
	update_Config();

	return true;
}

bool Flatscan::get_Emergency() {
	//Build CMD
	frame->cmd.clear();
	frame->cmd.push_back(0x6E);
	frame->cmd.push_back(0xc3);

	//Build DATA
	frame->data.clear();

	//Build SYNC - after DATA because we need to know the size of frame
	frame->computeSync();

	//Build CHK - crc16
	frame->computeCHK();

	////Send&Read
	try{
		Command::sendFrameReadAck(this);
	} catch(std::string e){
		std::cout << e << std::endl;
	}

	//Mean
	printf("\nFlatscan::get_Emergency\n");
	printf("\tD6->D7 (%d %d)\n", frame->data[6], frame->data[7]);
	printf("\tD8->D9 (%d %d)\n", frame->data[8], frame->data[9]);

	return true;
}

bool Flatscan::set_Led(LED led) {
	//Build CMD
	frame->cmd.clear();
	frame->cmd.push_back(0x78);
	frame->cmd.push_back(0xc3);

	//Build DATA
	frame->data.clear();

	switch(led){
		case IS_INIT : //green fixed led
			frame->data.push_back(0x01);
			frame->data.push_back(0x02);
			frame->data.push_back(0x00);
			frame->data.push_back(0x01);
			break;
		case ERROR : //red @5hz
			frame->data.push_back(0x02);
			frame->data.push_back(0x01);
			frame->data.push_back(0x00);
			frame->data.push_back(0x05);
			break;
		default : //unkown state
		frame->data.push_back(0x01);
		frame->data.push_back(0x03);
		frame->data.push_back(0x00);
		frame->data.push_back(0x01);
	}

	//Build SYNC - after DATA because we need to know the size of frame
	frame->computeSync();

	//Build CHK - crc16
	frame->computeCHK();

	//Send&Read
	try{
		Command::sendFrameReadAck(this);
	} catch(std::string e){
		std::cout << e << std::endl;
	}

	return true;
}

bool Flatscan::get_Measurments(bool reverse) {
	//Build CMD
	frame->cmd.clear();
	frame->cmd.push_back(0x5b);
	frame->cmd.push_back(0xc3);

	//Build DATA
	frame->data.clear();
	frame->data.push_back(this->config.Measurement_Mode);

	//Build SYNC - after DATA because we need to know the size of frame
	frame->computeSync();

	//Build CHK - crc16
	frame->computeCHK();

	//Send&Read
	try{
		Command::sendFrameReadAck(this);
	} catch(std::string e){
		std::cout << e << std::endl;
	}

	extractHeader();

	store_Measurments();

    return compute_Measurments(reverse);
}

void Flatscan::extractHeader() {
	int headerSize = 0;
		headerSize += this->config.CAN_FrameCntr * 6;
		headerSize += this->config.CTN * 2;
		headerSize += this->config.Facet_Nbr * 1;

		for(int i = 0; i < headerSize; i++) {
			header.push_back(frame->data[i]);
		}

	this->frame->data.erase(this->frame->data.begin(), this->frame->data.begin() + headerSize);
}

bool Flatscan::store_Measurments() {
	this->rawDistance.clear();
	this->rawRemission.clear();

	switch(this->config.MDI_Mode){
		case DISTANCE :
			this->rawDistance.clear();

			this->rawDistance = this->frame->data;
			break;
		case REMISSION :
			this->rawRemission.clear();

			this->rawRemission = this->frame->data;
			break;
		case DISTANCE_REMISSION :
			this->rawDistance.clear();
			remission.clear();

			this->rawDistance.insert(this->rawDistance.begin(), this->frame->data.begin(), this->frame->data.begin() + 800);
			this->rawRemission.insert(this->rawRemission.begin(), this->frame->data.begin() + this->frame->data.size() / 2, this->frame->data.end());

			break;
		default :
			throw string("Error in store_Measurments()");
	}

	return true;
}

bool Flatscan::compute_Measurments(bool reverse) {
	this->cloudPoints.clear();
	this->distance.clear();

    int dist, i;
    float angle;

    if(reverse)
        std::reverse(this->rawDistance.begin(), this->rawDistance.end());

	//No need to verify the MDI_Mode because we've already considered it in store_Measurments()
    //The size would be eqaul to 0 so no loop
    for(auto it = this->rawDistance.cbegin(); it != this->rawDistance.cend()-2; std::advance(it, 2)) {
        i = it - this->rawDistance.cbegin();

        if(reverse)
            dist = (*(it+1)) + (*it)*256;
        else
            dist = (*it) + (*(it+1))*256;

        angle = i/2 * this->angleStep;

        this->cloudPoints.push_back(Rect2(dist * cos(angle), (float) dist * sin(angle)));
        this->distance.push_back(dist);
    }

	for(int i = 0; i < (int)this->rawRemission.size() - 1; i+=2) {
			this->remission[i] = this->rawRemission[i+1]*256 + this->rawRemission[i];
	}

	return false;
}

bool Flatscan::get_ClosestObject(Measurement* m) {
	if(this->config.MDI_Mode == REMISSION){throw string("Cannot get closest if set as REMISSION !");}

    get_Measurments(false);

	int min = this->distance[0];
	int i, j;

	for(int i = 1; i < this->distance.size(); i++)
	{
		if (this->distance[i] < min) {
			min = this->distance[i];
			j = i;
		}
	}

	m->dist = min;
	m->angle = j * this->angleStep;

	if(this->config.MDI_Mode == DISTANCE_REMISSION)
		m->remission = this->remission[j*2+1]*256 + this->remission[j*2];

	return false;
}

bool Flatscan::get_MeasurmentsRect(vector<Rect2> *pts, bool reverse) {
	if(this->config.MDI_Mode == REMISSION){throw string("Cannot get Rect if set as REMISSION");}

        get_Measurments(reverse);

		*pts = this->cloudPoints;

		return false;
}
